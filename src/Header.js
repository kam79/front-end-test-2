import React from 'react';

let headerStyle = {
    backgroundImage: "url('https://files.asset.tv/live/s3fs-public/channel/untitled-17.png')",
    backgroundColor: "#000",
    backgroundSize: "contain"

}

const Header = (props) => {
    return (
        <header className="container" style={headerStyle}>
            <h1>{props.firstTitle}</h1>
            <HeaderTabs />
        </header>
    )
}

// Header tabs
const HeaderTabs = (props) => {
    return (
        <ul className="nav nav-tabs header-tabs">
            <li className="nav-item"><a className="nav-link active" href="#">Tab 1</a></li>
            <li className="nav-item"><a className="nav-link" href="#">Tab 2</a></li>
            <li className="nav-item"><a className="nav-link" href="#">Tab 3</a></li>
        </ul>
    )
} 

export default Header; 