import React from 'react';
import Filter1 from './Filter1';
import Filter2 from './Filter2';
import VideoResults from './VideoResults';

// Main body
const MainBody = (props) => {
    return (
        <div className="container" role="main">
            <h2>{props.secondTitle}</h2>
            <div className="row">
                <ul className="filters">
                    <li><Filter1 /></li>
                    <li><Filter2 /></li>
                </ul>
            </div>
            <VideoResults />
        </div>
    )
}

// Filters 
const FilterContainer = (props) => {
    return (
        <div className="row">
            <ul>
                <li><Filter1 /></li>
                <li><Filter2 /></li>
            </ul>
        </div>
    )
}



export default MainBody; 