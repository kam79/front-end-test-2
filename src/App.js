import React from 'react';
import Header from './Header';
import MainBody from './MainBody';
import axios from 'axios';

const App = () => {

    return (
        <div className="body-container">
            <Header firstTitle="Masterclass" />
            <MainBody secondTitle="Latest" />
        </div>
    )
}

let getData = (props) => {
    axios.get('https://titan.asset.tv/api/channel-view-json/2240')
    .then(function (response) {
      console.log(response);
      console.log(response.data.mcd.title);
    })
    .catch(function (error) {
      console.log(error);
    });
}

getData();

export default App;